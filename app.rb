# encoding: utf-8

require 'bundler/setup'
require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/json'

require 'irkit'
require 'hue'
require 'active_record'
require './models/homekit.rb'
require "find"

set :bind, '0.0.0.0'

# Constans
LINK = 1
EXT_LINK = 2
IRKIT = 3
HUE = 4
PDF = 5
LINK_STR = 'カテゴリ'
EXT_LINK_STR = '外部リンク'
IRKIT_STR = 'IRKit'
HUE_STR = 'hue'
PDF_STR = 'PDF'
IRKIT_ADDRESS = '192.168.0.9'
IMG_FILE_PATH = 'assets/img/'

get '/' do
  @actions = Actions.where(parent_action: 'index')
  erb :index
end

get '/:action' do
  @actions = Actions.where(parent_action: params[:action])
  erb :index
end

get '/actions/setting' do
  load_setting()
  @mode = 'new'
  erb :setting
end

post '/actions/new' do
  actions = Actions.new
  actions.action = params[:action]
  actions.description = params[:description]
  actions.image = params[:image]
  actions.parent_action = params[:parent_action]
  actions.action_type = params[:action_type]
  actions.save
  redirect '/actions/setting'
end

post '/api/:device/:commands' do
  device = params[:device]
  irkit = IRKit::Device.new(address: IRKIT_ADDRESS)
  p params
  unless irkit
    STDERR.puts 'device not found'
    exit 1
  end

  commands = params[:commands].split(',')
  commands.each.with_index(1) do |command, i|
    irdata = IRKit::App::Data['IR'][command]
    res = irkit.post_messages(irdata)
    case res.code
    when 200
      puts "Success: #{command} to #{device}"
    else
      raise res
    end
    sleep 1 if i < commands.size
  end

end

def load_setting
  @parent_act_opt = Actions.where(parent_action:'index',action_type:LINK).uniq
  @act_opt = Options.where(group: 'action_type')
  @ir_opt = Array.new
  IRKit::App::Data["IR"].each do |k,v|
    @ir_opt.push(k)
  end
  @ir_opt = @ir_opt.sort
  @img_opt = Array.new
  Find.find("./public/assets/img/") {|f|
    next unless FileTest.file?(f)
    @img_opt.push(File.basename(f))
  }
  @img_opt = @img_opt.sort
end

post "/api/irkit" do
  request.body.rewind  # 既に読まれているときのため
  params = JSON.parse request.body.read
  irkit = IRKit::Device.new(address: IRKIT_ADDRESS)

  unless irkit
    STDERR.puts 'device not found'
    exit 1
  end

  commands = params['commands'].split(',')
  commands.each.with_index(1) do |command, i|
    irdata = IRKit::App::Data['IR'][command]
    res = irkit.post_messages(irdata)
    case res.code
    when 200
      puts "Success: #{command} to #{device}"
    else
      raise res
    end
    sleep 1 if i < commands.size
  end
end

post "/api/hue" do
  request.body.rewind  # 既に読まれているときのため
  params = JSON.parse request.body.read

  client = Hue::Client.new
  unless client
    STDERR.puts 'device not found'
    exit 1
  end

  client.lights.each do |light|
    light.on!
  end

end

