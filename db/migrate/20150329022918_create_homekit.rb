class CreateHomekit < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.integer :action_type
      t.string :parent_action
      t.string :action
      t.string :description
      t.string :image
      t.integer :order
    end
    create_table :options do |t|
      t.string :group
      t.string :key
      t.string :value
    end
  end
end
