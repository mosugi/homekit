config = YAML.load_file( './database.yml' )
# 環境を切り替える
ActiveRecord::Base.establish_connection(config["db"]["development"])

class Actions < ActiveRecord::Base; end
class Options < ActiveRecord::Base; end
